---
layout: markdown_page
title: "Applying"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## How to Apply for a Position

The best way to apply for a position with GitLab is directly through our [jobs page](/jobs), where our open positions are advertised. If you do not see a job that aligns with your skillset, please keep your eye on the jobs page and check back in the future as we do add roles regularly. Please be advised that we do not retain unsolicited resumes on file, so you will need to apply directly to any position you are interested in now or in the future.

To apply for a current vacancy:

1. Go to our [jobs page](/jobs) and [view our open opportunities](/jobs/apply)
1. Click on the position title that interests you! Please also refer to the [country hiring guidelines](/jobs/faq/#country-hiring-guidelines) to see if we are able to hire in your location.
1. You will be redirected to the vacancy description and application form, where you will be asked to fill out basic personal information, provide your resume, LinkedIn, and/or cover letter, and answer any application questions, as well as answer a voluntary Equal Employment Opportunity questionnaire. While the EEO questionnaire has `US` in its title, it's open to all applicants from around the world.
1. Once you have finished, click "Submit Application" at the bottom.
1. Should you reach out to any GitLabbers on LinkedIn instead of or in addition to applying to our jobs page, you'll receive the following reply:

> Thank you for your interest in GitLab. We would much rather prefer you apply for the position you have in mind directly via our [Jobs page](https://about.gitlab.com/jobs/). This will ensure the right GitLabber reviews your profile and reverts back to you! Unfortunately at this time, I can not refer you for the position as we have not had a chance to work together. To ensure we stay [Inclusive](https://about.gitlab.com/handbook/values/#diversity), I can also not influence your application".

## The next step

We respond to all applications as soon as we're able, even if we decide not to proceed with A candidate is welcome to contact the recruiting team at any time for an update on their candidacy. In the meantime, you may want to [review the typical hiring timeline](/handbook/hiring/interviewing/#typical-hiring-timeline).
