---
layout: markdown_page
title: "Code Contributor Program Email templates"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----
### Direct email messaging
Below are email templates that can be used for communicating with community members. Whenever possible, it is strongly encourage that each email message be customized for each individual & circumstance.

#### Outreach after the first merged MR

```
Hello NAME,

I’m reaching out to both thank and congratulate you on your first Merge Request (MR) to help improve GitLab.  We appreciate everyone's effort to contribute to the GitLab, especially when it's an individual initiative and we are blessed to have such a wonderful community.  I wanted to offer you a couple of things as you’re getting started with your code contributions to GitLab.

1. Let us know if you'd like to receive the latest GitLab mug with a special hashtag to celebrate your first merged MR.  Please go to [URL] to submit your order (the quantity should be 1), and we will take care of the rest.  When you receive the merchandise, it would be great if you can make a post on twitter with your photo of the mug plus '@gitlab' & '#myfirstMRmerged' in the tweet.
2. Please let me know if you’d like to be paired with one of the experienced GitLab community members (a mentor) for a few weeks to help with your future contributions to GitLab.  

Thanks again for your first MR to GitLab and welcome to the GitLab community!

Sincerely,
YOUR_NAME
```

#### Outreach to inactive contributors

```
Hello NAME,

I’m reaching out to you as we have not seen a merged MR from you since DATE.  The GitLab community would not be the same if it weren’t for contributors like you.  If you haven’t done so lately, you can look for issues that are “Accepting merge requests” as we would welcome your help on these.

Please let me know if you have any questions and I look forward to your continued involvement in the GitLab community.  

Sincerely,
YOUR_NAME
```
