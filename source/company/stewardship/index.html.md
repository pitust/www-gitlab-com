---
layout: markdown_page
title: Stewardship
---

- TOC
{:toc}

## Our stewardship of GitLab CE

### Business model

GitLab Inc. is a for profit company that balances the need to improve GitLab
Community Edition (CE) with the need to add features to GitLab Enterprise
Edition (EE) exclusively in order to generate income. We have an [open
core](https://en.wikipedia.org/wiki/Open_core) business model and generate
almost all our revenue with [subscriptions to paid tiers](/pricing/). We
recognize that we need to balance the need to generate income and with the
needs of the open source project.

We have tried different business models, and many didn’t work. As a company, we realized we needed recurring revenue to continue our mission, and we introduced Enterprise Edition. Initially, everyone was worried innovation would stop on the Community Edition, but within six months, the Enterprise Edition was under the MIT license, and the community saw we were building both products, which helped us to build trust.

Our customers became confused, so after six months the arrangement stopped, and response was positive about the shift. We strive to keep improving both editions and to keep the promises below.


### Promises

We promise that:

1. We won't remove features from the open source codebase in order to make the same feature paid (features might be removed from the open source codebase due to changes in the application)
1. We won't introduce features into the open source codebase with a fixed delay, if a feature is planned to land in both it will be released simultaneously in both
1. We will always release and open source all tests that we have for a open source feature
1. The open source codebase will have all the features that are essential to running a large 'forge' with public and private repositories
1. The open source codebase will not contain any artificial limits (repositories, users, size, performance, etc.)
1. All stages of the DevOps lifecycle (plan, create, verify, package, release, configure, monitor) will have some open source features.
1. The majority of new features made by GitLab Inc. will be open source
1. The product will be available for download [without leaving an email address](https://news.ycombinator.com/item?id=17804916)
1. We will always allow you to [benchmark the performance](https://news.ycombinator.com/item?id=18103162) of GitLab.

### What features are paid-only

If the wider community contributes a new feature they get to choose if is open source or source-available (propietary and paid) by sending labeling merge request with the [tier](/handbook/marketing/product-marketing/#tiers) they prefer. If the wider community contributes a feature that is currently source-available we use the process linked in
[Contributing an EE-only feature to CE](#contributing-an-ee-only-feature-to-ce).

When GitLab Inc. makes a new feature we ask ourselves [who is the likely type buyer](/handbook/ceo/pricing/#the-likely-type-of-buyer-determines-what-features-go-in-what-tier) to determine what tier the feature goes into.
If the likely buyer is an individual contributor the feature will be open source, otherwise it will be source-available.

There aren't any features that are only useful to managers, directors, and executives.
So for every source-available feature there will be an individual contributor that might need it.
We're not saying that there aren't any individual contributors that need the feature,
just that we think that other buyers are relatively more likely to need it.
The more of GitLab that you use the more likely it is that you benefit from a higher tier.
Even a single person using GitLab might be best off using our highest tier.

It is hard to get [the tier](/handbook/marketing/product-marketing/#tiers) right, and if we put something in a tier that is too high we won't hesitate to [open-source](/2016/12/24/were-bringing-gitlab-pages-to-community-edition/) [it](https://news.ycombinator.com/item?id=10931347) or move it to a lower tier. We listen to our community in order to find what we feel is the right balance, and we iterate and make changes based on their feedback. At the same time, the premium product needs to hold value, and we believe we proide that.

All stages of the [DevOps lifecycle](/direction/#scope) are available in GitLab CE. There are companies using GitLab unpaid with more than 10,000 users.

If people ask us why a certain feature is paid we might reply with a [link to this section of the handbook](/stewardship#what-features-are-paid-only). We do not mean to imply you don't need the feature. Feel free to make the argument for moving it to another tier, we're listening.

### What features are in each paid tier?

[The likely type of buyer determines what features go in what tier](handbook/ceo/pricing/#the-likely-type-of-buyer-determines-what-features-go-in-what-tier).

### Why release simultaneously in both

Sometimes people suggest having features in EE for a limited time.
An example of a limited time release strategy is the [Business Source License](https://mariadb.com/bsl) that keeps features proprietary for 3 years.

At GitLab we want to give everyone access to most of the features (and all the essential ones) at the date they are announced.
We want to give people the option to both run and contribute to an open source edition that is maintained and that includes the most recent security fixes.

From time to time we do open source a feature that used to be EE only.
We do this when we realize that we've made a mistake applying our criteria, for example
when we learned that a branded homepage was an [essential feature](https://news.ycombinator.com/item?id=10931347) or
when we [brought GitLab Pages to the Community Edition](/2016/12/24/were-bringing-gitlab-pages-to-community-edition/).

Our plan is to become the most popular tool for people’s own git hosting service; we’ve managed that so far. Secondarily, we want to get to be the one with the most revenue. Thirdly, we want to become the most popular tool for hosting private repos. Once we’ve reached that, we want to be the most popular tool for hosting public repos. And, lastly, we want to be the number one tool for people to host not just code but books, tech papers, visual models, movies, etc. More info on this is on our [strategy page](/strategy/)

### How open source benefits from open core

GitLab Inc. has an open core business model that includes source-available code and selling subscriptions.
This benfits the the open source part of GitLab in the following ways:

1. New features being made by GitLab Inc. that are open source
1. [Responsible disclosure](/security/disclosure/) process and security fixes
1. Release management including a monthly release and patches
1. Packaging GitLab in our [Omnibus packages](https://gitlab.com/gitlab-org/omnibus-gitlab)
1. Running a [packages server](https://packages.gitlab.com/gitlab/)
1. Dependency upgrades (Rails, gems, etc.)
1. Performance improvements

### Existing contributed open source features will not become source-available

[We never move existing features already in CE, into EE](#promises). This applies regardless of whether
the feature was created by GitLab Inc. or community contributors. On occasion, the reverse
does happen where we open source a previously source-available feature.

### Contributing an existing EE feature to CE

When someone contributes an _existing_ EE feature to CE, we have a hard
decision to make. We encourage contributors to focus on new features not
already existing in CE and EE, so that both CE and EE editions of GitLab
benefit from the feature, and we can avoid any difficult decisions.

We encourage contributors visit our [direction page](/direction/)
to see features that are welcomed and also review [CONTRIBUTING.md](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md).

When someone contributes an _existing_ EE feature to CE, we weigh a number of factors to decide in accepting it, including:

1. What is the quality of the submitted code?
1. Is it a complete replacement of the EE functionality?
1. Does it meet the [contribution acceptance criteria](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria)?
1. Is it [more relevant for mid-market organizations or larger](/handbook/product/#paid-tiers)?
1. Is the person or organization submitting this using GitLab in an [SMB](/handbook/sales/#market-segmentation)?
1. Did the person or organization submitting this contribute to GitLab before?
1. Is it something that many of our existing customers chose GitLab Enterprise Edition for?
1. Is it relevant for running a large open source forge?
1. Is it an original work or based on the EE code?
1. Is there an actively maintained fork that incorporates this?
1. How many organizations are using this code in production?
1. How frequently has this functionality been requested for CE and by whom?

We'll weigh all factors and you can judge our stewardship of CE based on the outcome. As of July 22, 2016, we had only two cases: One had low code quality and the other one copied the EE code down to the last space. If you find these or other examples please link them here so people can get an idea of the outcome.

In case we're not sure, we'll consult with the [core team](/core-team/) to reach a conclusion.

### Contributing a not-yet-existing EE feature

When someone contributes a _not yet existing_ feature on the [EE issue tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues),
and it has met the [contribution acceptance criteria](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria),
we will accept it in whatever edition (CE or EE) they contribute to, provided that GitLab Inc. has not already started on working on the feature. (The contribution
should not contain any _already existing_ EE features in it.) We encourage contributors to @-mention the [relevant product manager](/handbook/product/#who-to-talk-to-for-what) earlier in the development process (in the issue or merge request) to ensure GitLab team members are not already working on the feature in order to avoid conflicts.
